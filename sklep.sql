-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 14 Lis 2018, 11:24
-- Wersja serwera: 10.1.28-MariaDB
-- Wersja PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `sklep_iih`
--

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `adresy_osob`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `adresy_osob`;
CREATE TABLE `adresy_osob` (
`imie` varchar(25)
,`nazwisko` varchar(35)
,`pesel` char(11)
,`Nazwa miejscowości` varchar(60)
,`Kod pocztowy` char(6)
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dostawcy`
--

DROP TABLE IF EXISTS `dostawcy`;
CREATE TABLE `dostawcy` (
  `id_dostawca` bigint(20) UNSIGNED NOT NULL,
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `id_firma` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `firmy`
--

DROP TABLE IF EXISTS `firmy`;
CREATE TABLE `firmy` (
  `id_firma` bigint(20) UNSIGNED NOT NULL,
  `id_kod_pocztowy` bigint(20) UNSIGNED NOT NULL,
  `id_ulica` bigint(20) UNSIGNED NOT NULL,
  `nip` char(15) NOT NULL,
  `regon` char(8) NOT NULL,
  `platnik_vat` tinyint(1) NOT NULL,
  `nazwa` varchar(70) NOT NULL,
  `nazwa_skrot` char(10) NOT NULL,
  `nr_posesja` varchar(5) NOT NULL,
  `nr_lokal` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klienci`
--

DROP TABLE IF EXISTS `klienci`;
CREATE TABLE `klienci` (
  `id_klient` bigint(20) UNSIGNED NOT NULL,
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `id_firma` bigint(20) UNSIGNED DEFAULT NULL,
  `saldo` decimal(10,2) NOT NULL,
  `data_otwarcia` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kody_pocztowe`
--

DROP TABLE IF EXISTS `kody_pocztowe`;
CREATE TABLE `kody_pocztowe` (
  `id_kod_pocztowy` bigint(20) UNSIGNED NOT NULL,
  `nazwa` char(6) NOT NULL,
  `id_miejscowosc` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `kody_pocztowe`
--

INSERT INTO `kody_pocztowe` (`id_kod_pocztowy`, `nazwa`, `id_miejscowosc`) VALUES
(1, '42-200', 1),
(2, '00-347', 9),
(3, '12-347', 3),
(4, '12-456', 3),
(5, '67-100', 2),
(6, '17-114', 3),
(7, '44-987', 6),
(8, '11-347', 7),
(9, '90-347', 10),
(10, '77-347', 8);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `miejscowosci`
--

DROP TABLE IF EXISTS `miejscowosci`;
CREATE TABLE `miejscowosci` (
  `id_miejscowosc` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `miejscowosci`
--

INSERT INTO `miejscowosci` (`id_miejscowosc`, `nazwa`) VALUES
(1, 'Częstochowa'),
(2, 'Warszawa'),
(3, 'Poznań'),
(4, 'Katowice'),
(5, 'Gdańsk'),
(6, 'Rzesów'),
(7, 'Kalisz'),
(8, 'Jelenia Góra'),
(9, 'Bielsko-Biała'),
(10, 'Białystok');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `osoby`
--

DROP TABLE IF EXISTS `osoby`;
CREATE TABLE `osoby` (
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `imie` varchar(25) NOT NULL,
  `nazwisko` varchar(35) NOT NULL,
  `pesel` char(11) NOT NULL,
  `id_kod_pocztowy` bigint(20) UNSIGNED NOT NULL,
  `id_ulica` bigint(20) UNSIGNED NOT NULL,
  `nr_posesja` varchar(5) NOT NULL,
  `nr_mieszkanie` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `osoby`
--

INSERT INTO `osoby` (`id_osoba`, `imie`, `nazwisko`, `pesel`, `id_kod_pocztowy`, `id_ulica`, `nr_posesja`, `nr_mieszkanie`) VALUES
(1, 'Alicja', 'Iksińska', '88888888888', 1, 1, '12', '5d'),
(2, 'Oliwia', 'Iksińska', '88888888888', 7, 1, '12', '5d');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pracownicy`
--

DROP TABLE IF EXISTS `pracownicy`;
CREATE TABLE `pracownicy` (
  `id_pracownik` bigint(20) UNSIGNED NOT NULL,
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `id_stanowisko` bigint(20) UNSIGNED NOT NULL,
  `data_zatrudnienia` date NOT NULL,
  `data_zwolnienia` date NOT NULL,
  `mnoznik_pensji` float NOT NULL,
  `cv` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rodzaje_urlopow`
--

DROP TABLE IF EXISTS `rodzaje_urlopow`;
CREATE TABLE `rodzaje_urlopow` (
  `id_typ_urlopu` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(30) NOT NULL,
  `opis` text NOT NULL,
  `sredni_czas_trwania` smallint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stanowiska`
--

DROP TABLE IF EXISTS `stanowiska`;
CREATE TABLE `stanowiska` (
  `id_stanowisko` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(30) NOT NULL,
  `pensja` decimal(10,2) NOT NULL,
  `opis` text NOT NULL,
  `nazwa_skrocona` char(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `towary`
--

DROP TABLE IF EXISTS `towary`;
CREATE TABLE `towary` (
  `id_towar` int(11) NOT NULL,
  `nazwa` varchar(60) NOT NULL,
  `nazwa_skrot` char(10) NOT NULL,
  `cena_zakupu_netto` decimal(10,2) NOT NULL,
  `data_zakupu` date NOT NULL,
  `id_kategoria` bigint(20) UNSIGNED NOT NULL,
  `stawka_vat` tinyint(3) UNSIGNED NOT NULL,
  `marza` smallint(3) UNSIGNED NOT NULL,
  `id_dostawca` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ulice`
--

DROP TABLE IF EXISTS `ulice`;
CREATE TABLE `ulice` (
  `id_ulica` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `ulice`
--

INSERT INTO `ulice` (`id_ulica`, `nazwa`) VALUES
(1, 'Fiołkowa');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `urlopy`
--

DROP TABLE IF EXISTS `urlopy`;
CREATE TABLE `urlopy` (
  `id_urlop` bigint(20) UNSIGNED NOT NULL,
  `id_pracownik` bigint(20) UNSIGNED NOT NULL,
  `data_start` date NOT NULL,
  `ilosc_dni` smallint(3) UNSIGNED NOT NULL,
  `id_typ` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura widoku `adresy_osob`
--
DROP TABLE IF EXISTS `adresy_osob`;

CREATE VIEW `adresy_osob`  AS  select `osoby`.`imie` AS `imie`,`osoby`.`nazwisko` AS `nazwisko`,`osoby`.`pesel` AS `pesel`,`miejscowosci`.`nazwa` AS `Nazwa miejscowości`,`kody_pocztowe`.`nazwa` AS `Kod pocztowy` from ((`osoby` join `miejscowosci`) join `kody_pocztowe`) where ((`osoby`.`id_kod_pocztowy` = `kody_pocztowe`.`id_kod_pocztowy`) and (`kody_pocztowe`.`id_miejscowosc` = `miejscowosci`.`id_miejscowosc`)) ;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `dostawcy`
--
ALTER TABLE `dostawcy`
  ADD UNIQUE KEY `id_dostawca` (`id_dostawca`),
  ADD KEY `id_osoba` (`id_osoba`),
  ADD KEY `id_firma` (`id_firma`);

--
-- Indexes for table `firmy`
--
ALTER TABLE `firmy`
  ADD UNIQUE KEY `id_firma` (`id_firma`),
  ADD KEY `id_kod_pocztowy` (`id_kod_pocztowy`),
  ADD KEY `id_ulica` (`id_ulica`);

--
-- Indexes for table `klienci`
--
ALTER TABLE `klienci`
  ADD UNIQUE KEY `id_klient` (`id_klient`),
  ADD KEY `klienci_osoby_index` (`id_osoba`),
  ADD KEY `id_firma` (`id_firma`);

--
-- Indexes for table `kody_pocztowe`
--
ALTER TABLE `kody_pocztowe`
  ADD UNIQUE KEY `id_kod_pocztowy` (`id_kod_pocztowy`),
  ADD KEY `id_miejscowosc` (`id_miejscowosc`);

--
-- Indexes for table `miejscowosci`
--
ALTER TABLE `miejscowosci`
  ADD UNIQUE KEY `id_miejscowosc` (`id_miejscowosc`);

--
-- Indexes for table `osoby`
--
ALTER TABLE `osoby`
  ADD UNIQUE KEY `id_osoba` (`id_osoba`),
  ADD KEY `id_kod_pocztowy` (`id_kod_pocztowy`),
  ADD KEY `id_ulica` (`id_ulica`);

--
-- Indexes for table `pracownicy`
--
ALTER TABLE `pracownicy`
  ADD UNIQUE KEY `id_pracownik` (`id_pracownik`),
  ADD KEY `pracownicy_osoby_index` (`id_osoba`),
  ADD KEY `pracownicy_stanowiska_index` (`id_stanowisko`);

--
-- Indexes for table `rodzaje_urlopow`
--
ALTER TABLE `rodzaje_urlopow`
  ADD UNIQUE KEY `id_typ_urlopu` (`id_typ_urlopu`);

--
-- Indexes for table `stanowiska`
--
ALTER TABLE `stanowiska`
  ADD UNIQUE KEY `id_stanowisko` (`id_stanowisko`);

--
-- Indexes for table `towary`
--
ALTER TABLE `towary`
  ADD KEY `id_dostawca` (`id_dostawca`);

--
-- Indexes for table `ulice`
--
ALTER TABLE `ulice`
  ADD UNIQUE KEY `id_ulica` (`id_ulica`);

--
-- Indexes for table `urlopy`
--
ALTER TABLE `urlopy`
  ADD UNIQUE KEY `id_urlop` (`id_urlop`),
  ADD KEY `id_typ` (`id_typ`),
  ADD KEY `id_pracownik` (`id_pracownik`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `dostawcy`
--
ALTER TABLE `dostawcy`
  MODIFY `id_dostawca` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `firmy`
--
ALTER TABLE `firmy`
  MODIFY `id_firma` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `klienci`
--
ALTER TABLE `klienci`
  MODIFY `id_klient` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `kody_pocztowe`
--
ALTER TABLE `kody_pocztowe`
  MODIFY `id_kod_pocztowy` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `miejscowosci`
--
ALTER TABLE `miejscowosci`
  MODIFY `id_miejscowosc` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `osoby`
--
ALTER TABLE `osoby`
  MODIFY `id_osoba` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `pracownicy`
--
ALTER TABLE `pracownicy`
  MODIFY `id_pracownik` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rodzaje_urlopow`
--
ALTER TABLE `rodzaje_urlopow`
  MODIFY `id_typ_urlopu` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `stanowiska`
--
ALTER TABLE `stanowiska`
  MODIFY `id_stanowisko` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ulice`
--
ALTER TABLE `ulice`
  MODIFY `id_ulica` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `urlopy`
--
ALTER TABLE `urlopy`
  MODIFY `id_urlop` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `dostawcy`
--
ALTER TABLE `dostawcy`
  ADD CONSTRAINT `dostawcy_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`),
  ADD CONSTRAINT `dostawcy_ibfk_2` FOREIGN KEY (`id_firma`) REFERENCES `firmy` (`id_firma`);

--
-- Ograniczenia dla tabeli `firmy`
--
ALTER TABLE `firmy`
  ADD CONSTRAINT `firmy_ibfk_1` FOREIGN KEY (`id_ulica`) REFERENCES `ulice` (`id_ulica`),
  ADD CONSTRAINT `firmy_ibfk_2` FOREIGN KEY (`id_kod_pocztowy`) REFERENCES `kody_pocztowe` (`id_kod_pocztowy`);

--
-- Ograniczenia dla tabeli `klienci`
--
ALTER TABLE `klienci`
  ADD CONSTRAINT `klienci_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`),
  ADD CONSTRAINT `klienci_ibfk_2` FOREIGN KEY (`id_firma`) REFERENCES `firmy` (`id_firma`);

--
-- Ograniczenia dla tabeli `kody_pocztowe`
--
ALTER TABLE `kody_pocztowe`
  ADD CONSTRAINT `kody_pocztowe_ibfk_1` FOREIGN KEY (`id_miejscowosc`) REFERENCES `miejscowosci` (`id_miejscowosc`);

--
-- Ograniczenia dla tabeli `osoby`
--
ALTER TABLE `osoby`
  ADD CONSTRAINT `kody_pocztowe_osoby_fk` FOREIGN KEY (`id_kod_pocztowy`) REFERENCES `kody_pocztowe` (`id_kod_pocztowy`),
  ADD CONSTRAINT `osoby_ibfk_1` FOREIGN KEY (`id_ulica`) REFERENCES `ulice` (`id_ulica`);

--
-- Ograniczenia dla tabeli `pracownicy`
--
ALTER TABLE `pracownicy`
  ADD CONSTRAINT `pracownicy_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`),
  ADD CONSTRAINT `pracownicy_ibfk_2` FOREIGN KEY (`id_stanowisko`) REFERENCES `stanowiska` (`id_stanowisko`);

--
-- Ograniczenia dla tabeli `towary`
--
ALTER TABLE `towary`
  ADD CONSTRAINT `towary_ibfk_1` FOREIGN KEY (`id_dostawca`) REFERENCES `dostawcy` (`id_dostawca`);

--
-- Ograniczenia dla tabeli `urlopy`
--
ALTER TABLE `urlopy`
  ADD CONSTRAINT `urlopy_ibfk_1` FOREIGN KEY (`id_typ`) REFERENCES `rodzaje_urlopow` (`id_typ_urlopu`),
  ADD CONSTRAINT `urlopy_ibfk_2` FOREIGN KEY (`id_pracownik`) REFERENCES `pracownicy` (`id_pracownik`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
